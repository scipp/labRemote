import sys

try:
    from skbuild import setup
except ImportError:
    print('Please update pip, you need pip 10 or greater,\n'
          ' or you need to install the PEP 518 requirements in pyproject.toml yourself', file=sys.stderr)
    raise

setup(
    name="labRemote",
    cmake_args=['-DUSE_PYTHON=on'],
    version='0.0.3',
    url='https://gitlab.cern.ch/berkeleylab/labRemote',
    author='Giordon Stark',
    author_email='gstark@cern.ch',
    description='Pip installable package of labRemote',
    packages=['labRemote'],
    package_dir={'': 'src'}
)
