from labRemote import chiller, com

import time
from argparse import ArgumentParser

import logging
logging.basicConfig()
logger = logging.getLogger("chiller_example")

def chiller_example(device, baud, temperature) :

    ##
    ## setup the communication line
    ##
    try :
        serial = com.TextSerialCom(device, baud)
    except :
        logger.error(f"Unable to initialize serial communication with device at port \"{device}\"")
        sys.exit(1)
    if serial is None :
        logger.error("Returned communication device is None")
        sys.exit(1)
    serial.setTermination("\r")

    ##
    ## setup the chiller device
    ##
    chiller = chiller.PolySciLM()
    chiller.setCom(serial)
    chiller.init()
    chiller.turnOn()
    chiller.setTargetTemperature(temperature)

    ##
    ## continous monitoring
    ##
    while True :
        temp = chiller.measureTemperature()
        set_temp = chiller.getTargetTemperature()
        logger.info(f"Set temperature: {set_temp:0.4f}, Measured temperature: {temp:0.4f}")
        time.sleep(1) # 1 second between measurements

if __name__ == "__main__" :
    parser = ArgumentParser(description = "labRemote libChiller example")
    parser.add_argument("-p", "--port", required = True, type = str,
        help = "Set the address of the port connected to the chiller (e.g. /dev/ttyUSB1)"
    )
    parser.add_argument("-b", "--baud", default = 9600, type = int,
        help = "Set the baud rate of the chiller"
    )
    parser.add_argument("-t", "--temperature", default = 15, type = float,
        help = "Set the target temperature of the chiller (degrees Celsius)"
    )
    args = parser.parse_args()

    chiller_example(args.port, args.baud, args.temperature)
