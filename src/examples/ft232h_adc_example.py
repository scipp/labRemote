from labRemote import devcom

from argparse import ArgumentParser
import sys, inspect
import logging
logging.basicConfig()
logger = logging.getLogger("ft232h_adc_example")

def ft232h_adc_example() :

    ##
    ## Create and initialize the FT232H device
    ##
    mpsse = devcom.MPSSEChip
    protocol = mpsse.Protocol.I2C
    speed = mpsse.Speed.FOUR_HUNDRED_KHZ
    endianness = mpsse.Endianness.MSBFirst
    ft232 = devcom.FT232H(protocol, speed, endianness, "", "")

    ##
    ## create two ADCs that are on the I2C bus, each with their specific FTDI I2C bus line
    ##
    adc0 = devcom.AD799X(2.5, devcom.AD799X.Model.AD7998, devcom.I2CFTDICom(ft232, 0x21))
    adc1 = devcom.AD799X(2.5, devcom.AD799X.Model.AD7998, devcom.I2CFTDICom(ft232, 0x22))

    vddd_chan = 0
    ntc_chan = 6
    print(31 * "-")
    for i in range(10) :
        print(f"[{i}] VDDD = {adc0.read(vddd_chan):.4f}, NTC = {adc1.read(ntc_chan):.4f}")
    print(31 * "-")

if __name__ == "__main__" :

    parser = ArgumentParser(description = "Example of communicating with an I2C-based ADC through the FTDI FT232H chip")
    parser.parse_args()

    ##
    ## first check that the FTDI support has been enabled in the current labRemote build
    ##
    if not inspect.isclass(devcom.I2CFTDICom) :
        logger.error("FTDI support has not been enabled")
        sys.exit(1)

    ft232h_adc_example()
