from labRemote import devcom

from argparse import ArgumentParser
import sys, inspect
import logging
logging.basicConfig()
logger = logging.getLogger("temp_hum_monitor_example")

def temp_hum_monitor_example() :

    logger.warning("Example not implemented!")

if __name__ == "__main__" :

    parser = ArgumentParser(description = "Example of performing continous monitoring of several climate sensors, using data streams")
    parser.parse_args()

    ##
    ## first check that the FTDI support has been enabled in the current labRemote build
    ##
    if not inspect.isclass(devcom.I2CFTDICom) :
        logger.error("FTDI support has not been enabled")
        sys.exit(1)

    temp_hum_monitor()
