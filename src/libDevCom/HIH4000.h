#ifndef HIH4000_H
#define HIH4000_H

#include "ClimateSensor.h"
#include "TextSerialCom.h"
#include "ADCDevice.h"

//! \brief An implementation of 'ClimateSensor' for reading an HIH4000 sensor
/**
 * Measures humidity using a HIH4000 sensor connected to a channel on an ADC device.
 * Humidity requires temperature connection: according sensor has to be provided.
 * A better measurement accuracy down to 3.5% RH can be achieved with the pre-calibrated
 * models HIH4000-003 and -004 with a datasheet providing offset and slope of Vout vs. RH 
 * calibrated with a supply voltage of 5V. The same supply voltage has to be used for the 
 * connected HIH4000.
 * HIH4000 specs: https://sensing.honeywell.com/HIH-4000-001-humidity-sensors
 */

class HIH4000: public ClimateSensor
{
public:
  /**
   * \param chan The channel number of the analog channel connected to the Pt
   * \param dev The ADCDevice for interfacing with the Adruino or similar - should be calibrated
   *        to return measured voltage as 0...100% of the operational voltage
   * \param tempSens The Temperature sensor used for the temperature compensation
   * \param Vsup The supply voltage
   */
  HIH4000(uint8_t chan, std::shared_ptr<ADCDevice> dev, std::shared_ptr<ClimateSensor> tempSens, float Vsup = 5.0);
  
  /**
   * \param chan The channel number of the analog channel connected to the Pt
   * \param dev The ADCDevice for interfacing with the Adruino or similar - should be calibrated
   *        to return measured voltage as 0...100% of the operational voltage
   * \param tempSens The Temperature sensor used for the temperature compensation
   * \param offset The zero offset of Vout vs RH of calibrated sensors at 5V
   * \param slope The Vout vs RH slope of calibrated sensors at 5V
   */
  HIH4000(uint8_t chan, std::shared_ptr<ADCDevice> dev, std::shared_ptr<ClimateSensor> tempSens, float offset, float slope);
  virtual ~HIH4000();

  virtual void init();
  virtual void reset();
  virtual void read();

  virtual float temperature() const;
  virtual float humidity() const;
  virtual float pressure() const;

private:
  //! Interface with the Arduino via the ADC protocol
  std::shared_ptr<ADCDevice> m_adcdev;
  //! The channel of the ADC connected to the Pt
  uint8_t m_chan;
  //! The temperature recorded in the last reading (defaults to -273.15)
  float m_temperature = -273.15;
  //! The humidity recorded in the last reading (defaults to 0)
  float m_humidity = 0.;
  //! Supply voltage
  float m_Vsup = 5.;
  //! Whether the calibration data of a sensor is used
  bool m_calib = false;
  //! Zero offset for calibrated sensor
  float m_offset = 0.85;
  //! Slope for calibrated sensor
  float m_slope = 0.03;
  //! ref. to the temperature sensor from which temp. correction is derived
  std::shared_ptr<ClimateSensor> m_tempSens;
};

#endif // HIH4000_H
