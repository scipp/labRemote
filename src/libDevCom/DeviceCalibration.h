#ifndef DEVICECALIBRATION_H
#define DEVICECALIBRATION_H

#include <cstdint>

//! \brief Interface for converting counts to physical units
/**
 * The calibration of a device is responsible for converting digital counts
 * used by devices into corresponding physical values.
 *
 * There is no requirement that the physical value be the actual input/output
 * of the device. If the voltage value of interest is fed to an ADC input using
 * a voltage divider, then the calibration can already account for it.
 */
class DeviceCalibration
{
public:
  DeviceCalibration();
  virtual ~DeviceCalibration();

  //! \brief Convert counts to a physical value
  /**
   * \param counts Counts used by the device.
   *
   * \return Physical value
   */
  virtual double calibrate(int32_t counts) =0;

  //! \brief Convert physical value to counts
  /**
   * \param value Physical value
   *
   * \return Counts used by the device.
   */  
  virtual int32_t uncalibrate(double value) =0;
};

#endif // DEVICECALIBRATION_H
