#include "I2CFTDICom.h"
#include "MPSSEChip.h"

#include <string.h>
#include <unistd.h>

#include "ComIOException.h"

I2CFTDICom::I2CFTDICom(std::shared_ptr<MPSSEChip> mpsse, uint8_t deviceAddr)
  : I2CCom(deviceAddr)
{
  m_mpsse = mpsse;
}

I2CFTDICom::~I2CFTDICom()
{ }

void I2CFTDICom::write_reg32(uint32_t address, uint32_t data)
{
  std::vector<uint8_t> datavec = {static_cast<uint8_t>((data>>24)&0xFF),
				  static_cast<uint8_t>((data>>16)&0xFF),
				  static_cast<uint8_t>((data>> 8)&0xFF),
				  static_cast<uint8_t>((data>> 0)&0xFF)};

  write_block(address, datavec);
}

void I2CFTDICom::write_reg16(uint32_t address, uint16_t data)
{
  std::vector<uint8_t> datavec = {static_cast<uint8_t>((data>> 8)&0xFF),
				  static_cast<uint8_t>((data>> 0)&0xFF)};

  write_block(address, datavec);
}

void I2CFTDICom::write_reg8(uint32_t address, uint8_t data)
{
  std::vector<uint8_t> datavec = {static_cast<uint8_t>((data>> 0)&0xFF)};

  write_block(address, datavec);
}

void I2CFTDICom::write_reg32(uint32_t data)
{
  std::vector<uint8_t> datavec = {static_cast<uint8_t>((data>>24)&0xFF),
				  static_cast<uint8_t>((data>>16)&0xFF),
				  static_cast<uint8_t>((data>> 8)&0xFF),
				  static_cast<uint8_t>((data>> 0)&0xFF)};

  write_block(datavec);
}

void I2CFTDICom::write_reg16(uint16_t data)
{
  std::vector<uint8_t> datavec = {static_cast<uint8_t>((data>> 8)&0xFF),
				  static_cast<uint8_t>((data>> 0)&0xFF)};

  write_block(datavec);
}

void I2CFTDICom::write_reg8(uint8_t data)
{
  std::vector<uint8_t> datavec = {static_cast<uint8_t>((data>> 0)&0xFF)};

  write_block(datavec);
}

void I2CFTDICom::write_block(uint32_t address, const std::vector<uint8_t>& data)
{
    // Establish the I2C START condition, taking control of the I2C bus.
    if(!m_mpsse->start())
        throw ComIOException("I2CFTDICom cannot sent START.");

    // Address the I2C secondary (i.e. wake it up), telling it that a write will
    // happen (LSB is 0)
    char buf[1]={static_cast<int8_t>(deviceAddr()<<1)};
    if(!m_mpsse->write(buf, 1))
      throw ComIOException("I2CFTDICom cannot WRITE device address.");
    
    // Write the device's register address that we wish to read from
    buf[0]=static_cast<int8_t>(address&0xFF);
    if(!m_mpsse->write(buf, 1))
      throw ComIOException("I2CFTDICom cannot WRITE register address.");
    
    // Transmit the data over the I2C bus to the I2C secondary
    if(!m_mpsse->write(const_cast<char*>(reinterpret_cast<const char*>(&data[0])), data.size()))
      throw ComIOException("I2CFTDICom cannot WRITE data.");
    
    // Establish the I2C STOP condition on the bus, ending the current I2C transaction.
    if(!m_mpsse->stop())
      throw ComIOException("I2CFTDICom cannot send STOP.");
}

void I2CFTDICom::write_block(const std::vector<uint8_t>& data)
{
  // Establish the I2C START condition, taking control of the I2C bus.
  if(!m_mpsse->start())
    throw ComIOException("I2CFTDICom cannot sent START.");

  // Address the I2C secondary (i.e. wake it up), telling it that a write will
  // happen (LSB is 0)
  char buf[1]={static_cast<int8_t>(deviceAddr()<<1)};
  if(!m_mpsse->write(buf, 1))
    throw ComIOException("I2CFTDICom cannot WRITE device address.");

  // Transmit the data over the I2C bus to the I2C secondary
  if(!m_mpsse->write(const_cast<char*>(reinterpret_cast<const char*>(&data[0])), data.size()))
    throw ComIOException("I2CFTDICom cannot WRITE data.");

  // Establish the I2C STOP condition on the bus, ending the current I2C transaction.
  if(!m_mpsse->stop())
    throw ComIOException("I2CFTDICom cannot send STOP.");
}

uint32_t I2CFTDICom::read_reg32(uint32_t address)
{
  std::vector<uint8_t> data(4);
  read_block(address, data);
  return (data[0]<<24)|(data[1]<<16)|(data[2]<<8)|(data[3]<<0);
}

uint32_t I2CFTDICom::read_reg24(uint32_t address)
{
  std::vector<uint8_t> data(3);
  read_block(address, data);
  return (data[0]<<16)|(data[1]<<8)|(data[2]<<0);
}

uint16_t I2CFTDICom::read_reg16(uint32_t address)
{
  std::vector<uint8_t> data(2);
  read_block(address, data);
  return (data[0]<<8)|(data[1]<<0);
}

uint8_t I2CFTDICom::read_reg8(uint32_t address)
{
  std::vector<uint8_t> data(1);
  read_block(address, data);
  return (data[0]<<0);
}

uint32_t I2CFTDICom::read_reg32()
{
  std::vector<uint8_t> data(4);
  read_block(data);
  return (data[0]<<24)|(data[1]<<16)|(data[2]<<8)|(data[3]<<0);
}

uint32_t I2CFTDICom::read_reg24()
{
  std::vector<uint8_t> data(3);
  read_block(data);
  return (data[0]<<16)|(data[1]<<8)|(data[2]<<0);
}

uint16_t I2CFTDICom::read_reg16()
{
  std::vector<uint8_t> data(2);
  read_block(data);
  return (data[0]<<8)|(data[1]<<0);
}

uint8_t I2CFTDICom::read_reg8()
{
  std::vector<uint8_t> data(1);
  read_block(data);
  return (data[0]<<0);
}

void I2CFTDICom::read_block(uint32_t address, std::vector<uint8_t>& data)
{
  char buf[1] = {0};
  char *outbuf;

  // Establish the I2C START condition, taking control of the I2C bus.
  if(!m_mpsse->start())
    throw ComIOException("I2CFTDICom cannot sent START.");

  // Address the I2C secondary (i.e. wake it up), telling it that a write will
  // happen (LSB is 0)
  buf[0]=static_cast<int8_t>(deviceAddr()<<1);
  if(!m_mpsse->write(buf, 1))
    throw ComIOException("I2CFTDICom cannot WRITE device address.");

  // Write the device's register address that we wish to read from
  buf[0]=static_cast<int8_t>(address&0xFF);
  if(!m_mpsse->write(buf, 1))
    throw ComIOException("I2CFTDICom cannot WRITE register address.");

  // Read back the data, first checking that the I2C secondary responded to the address write
  // with ACK bit HIGH
  if(m_mpsse->get_ack())
    {
      // Take control of I2C bus
      if(!m_mpsse->start())
	    throw ComIOException("I2CFTDICom cannot sent START.");
      
      // Address the I2C secondary, telling it that a read will happen (LSB is 1)
      buf[0]=static_cast<int8_t>((deviceAddr()<<1)|1);
      if(!m_mpsse->write(buf, 1))
	    throw ComIOException("I2CFTDICom cannot WRITE device address.");

      // Set the I2C ACK bit high to allow continous clocking out of I2C data from
      // I2C secondary during the calls to read(...).
      m_mpsse->set_ack();
      outbuf = m_mpsse->read(data.size());
      for(uint32_t i=0;i<data.size();i++)
	    data[i] = outbuf[i];

      // Set the I2C ACK bit low to tell the I2C secondary that we are done
      // reading data, and then perform a single dummy read(...) so that
      // this NACK is clocked out from I2C primary to I2C secondary.
      m_mpsse->set_nack();
      m_mpsse->read(1);
    }
  else
    throw ComIOException("I2CFTDICom did not see an ACK.");

  // Establish the I2C STOP condition on the bus, ending the current I2C transaction.
  if(!m_mpsse->stop())
    throw ComIOException("I2CFTDICom cannot send STOP.");
}

void I2CFTDICom::read_block(std::vector<uint8_t>& data)
{
  char buf[1] = {0};
  char *outbuf;

  // Establish the I2C START condition, taking control of the I2C bus.
  if(!m_mpsse->start())
    throw ComIOException("I2CFTDICom cannot sent START.");

  // Read back the data, first checking that the I2C secondary responded to the address write
  // with ACK bit HIGH
  if(m_mpsse->get_ack())
    {
      // Take control of I2C bus
      if(!m_mpsse->start())
	    throw ComIOException("I2CFTDICom cannot sent START.");
      
      // Address the I2C secondary, telling it that a read will happen (LSB is 1)
      buf[0]=static_cast<int8_t>((deviceAddr()<<1)|1);
      if(!m_mpsse->write(buf, 1))
	    throw ComIOException("I2CFTDICom cannot WRITE device address.");

      // Set the I2C ACK bit high to allow continous clocking out of I2C data from
      // I2C secondary during the calls to read(...).
      m_mpsse->set_ack();
      outbuf = m_mpsse->read(data.size());
      for(uint32_t i=0;i<data.size();i++)
	    data[i] = outbuf[i];

      // Set the I2C ACK bit low to tell the I2C secondary that we are done
      // reading data, and then perform a single dummy read(...) so that
      // this NACK is clocked out from I2C primary to I2C secondary.
      m_mpsse->set_nack();
      m_mpsse->read(1);
    }
  else
    throw ComIOException("I2CFTDICom did not see an ACK.");

  // Establish the I2C STOP condition on the bus, ending the current I2C transaction.
  if(!m_mpsse->stop())
    throw ComIOException("I2CFTDICom cannot send STOP.");
}
