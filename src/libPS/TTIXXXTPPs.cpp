#include "TTIXXXTPPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(TTIXXXTPPs)

TTIXXXTPPs::TTIXXXTPPs(const std::string& name) :
TTIPs(name, {"MX180TP","QL355TP"}, 3)
{ }
