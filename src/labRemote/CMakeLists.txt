file(GLOB REGISTER_PYTHON_MODULE_SRC
          CONFIGURE_DEPENDS
          "${PROJECT_SOURCE_DIR}/src/*/python.cpp"
          )

set(python_module_name _labRemote)
set(labremote_libs Com Utils PS EquipConf DataSink DevCom Chiller)

pybind11_add_module(${python_module_name} module.cpp ${REGISTER_PYTHON_MODULE_SRC})
target_link_libraries(${python_module_name} PRIVATE ${labremote_libs} pybind11_json)

if (SKBUILD)
  # Set the RPATH
  if (APPLE)
    set(rpath "@loader_path/../../..")
  else()
    set(rpath "$ORIGIN/../../..")
  endif()
  message("${rpath}")
  set_target_properties(${python_module_name} PROPERTIES INSTALL_RPATH ${rpath})
endif()

set(PYLABREMOTE_INSTALL_PATH ${CMAKE_INSTALL_LIBDIR}/python${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR}/site-packages/labRemote)
install(TARGETS ${python_module_name} LIBRARY DESTINATION ${PYLABREMOTE_INSTALL_PATH} )
install(FILES __init__.py com.py ps.py ec.py DESTINATION ${PYLABREMOTE_INSTALL_PATH} )
