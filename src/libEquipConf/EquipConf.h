#ifndef EQUIPCONF__H
#define EQUIPCONF__H

#include <string>
#include <unordered_map>

#include <nlohmann/json.hpp>

#include "IPowerSupply.h"
#include "PowerSupplyChannel.h"

#include "ICom.h"

/** \brief Factory for discovering hardware resources and instantiate/configure controllers. 
 *
 * Valid JSON hardware config file is made up of two blocks:
 *  - devices: physical devices registered in a registry
 *  - channels: logical channels 
 */
class EquipConf
{
public:
  /** Configuration @{ */
  
  /** Constructor */
  EquipConf();

  /** Constructor
   * @param hardwareConfigFile input JSON file with list of hardware resources and options
   */
  EquipConf(const std::string& hardwareConfigFile);

  /** Constructor
   * @param hardwareConfig JSON object with list of hardware sources and options
   */ 
  EquipConf(const nlohmann::json& hardwareConfig);

  ~EquipConf();

  /** Set input hardware list file
   * @param hardwareConfigFile input JSON file with list of hardware resources and options
   */
  void setHardwareConfig(const std::string& hardwareConfigFile);

  /** Set input hardware list file
   * @param hardwareConfig input JSON object with list of hardware resources and options
   */
  void setHardwareConfig(const nlohmann::json& hardwareConfig);

  /** Get device JSON configuration 
      @param label device name
      @return device JSON configuration (by reference)
   */  
  nlohmann::json getDeviceConf(const std::string& label);

  /** Get channel JSON configuration 
      @param label channel name
      @return channel JSON configuration (by reference)
   */  
  nlohmann::json getChannelConf(const std::string& label);
  
  /** @} */

public:
  /** Get (or create) hardware handles (by label) 
   * @{ 
   */

  /** Get Power-Supply object corresponding to name
   *
   * If this is the first time the power supply name is requested:
   *  1. Create the object
   *  2. Call IPowerSupply::connect()
   *  3. Call IPowerSupply::checkCompatibilityList()
   *
   * @param name label for the hardware object in JSON configuration file (*not* the model)
   */
  std::shared_ptr<IPowerSupply> getPowerSupply(const std::string& name);  

  /** Get Power-Supply channel object; if it was not instantiated yet, create the object.
   *
   * The power supply itself will also be instiated following `getPowerSupply`.
   *
   * @param name label for the channel object in JSON configuration file
   */
  std::shared_ptr<PowerSupplyChannel> getPowerSupplyChannel(const std::string& name);  

  /** @} */
  
private:

  /// JSON file with hardware list and options (see example input-hw.nlohmann::json file for syntax)
  nlohmann::json m_hardwareConfig;

  /// Stored handles of IPowerSupply pointers created
  std::unordered_map<std::string, std::shared_ptr<IPowerSupply>> m_listPowerSupply;

  /// Stored handles of PowerSupplyChannel pointers created
  std::unordered_map<std::string, std::shared_ptr<PowerSupplyChannel>> m_listPowerSupplyChannel;

  /** \brief Create and initialize a ICom object based on JSON settings
   *
   * The `ComRegistry` is used to look up the `ICom` class
   * corresponding to the "protocol" field of the config object.
   * If no such class exists, an exception is thrown.
   *
   * \param communication JSON object containing
   */
  std::shared_ptr<ICom> createCommunication(const nlohmann::json& config) const;
};

#endif
